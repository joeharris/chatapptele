# README #

### What is this repository for? ###

* A quick Python chat app using UDP for instant messaging and TCP for file transfer. 
* Note: User registration server code supplied.

### How do I get set up? ###

* The main file is chatApp.py
* chatRegServ.c will allow you to register to the IP address of whatever machine you run this file on.
* Make sure there is no NAT interference as NAT has not been handled by this due to the 3-point exchange.