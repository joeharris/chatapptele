#ifdef _WIN32

#include "tim_sockets.h"

// Ensure WSAStartup() and WSACleanup() are called by including them in constructor/destructor for a global object
class winsock {
  public:
	winsock();
	~winsock();
} tim_sockets;

winsock::winsock()
{
    WORD wVersionRequested;
    WSADATA wsaData;
    wVersionRequested = MAKEWORD(2, 2);
    WSAStartup(wVersionRequested, &wsaData);
}

winsock::~winsock()
{
	WSACleanup();
}

#endif
