// Wrapper for sockets functions that allows programs to run on both Windows and Unix.
// Copyright (C) Tim Moors, 2009.  Email moors AT ieee DOT org
#ifndef TIM_SOCKETS_H
#define TIM_SOCKETS_H 

/*
Wrapper				Linux		Windows
SOCKET				int			SOCKET
close				close		closesocket
ioctl				ioctl		ioctlsocket
inet_ntop			inet_ntop	InetNtop(Vista or later)
SHUT_RD				SHUT_RD		SD_RECEIVE
SHUT_WR				SHUT_WR		SD_SEND
SHUT_RDWR			SHUT_RDWR	SD_BOTH
getSocketError()	errno		WSAGetLastError()
INVALID_SOCKET		(-1)		INVALID_SOCKET		returned by socket()
SOCKET_ERROR		(-1)		SOCKET_ERROR		returned by most functions
*/

#ifdef _WIN32 // Microsoft Windows
#pragma comment (lib, "Ws2_32")  // Link in Ws2_32.lib

#  if !defined(_WINSOCK2API_) && !defined(_WINSOCKAPI_)
#    include <winsock2.h>
#  endif

#  include <ws2tcpip.h> // getaddrinfo(), freeaddrinfo(), getnameinfo() etc; NI_*, AI_*, addrinfo

#  define getSocketError WSAGetLastError

#  define close closesocket
#  define ioctl ioctlsocket
#  define inet_ntop InetNtop // Client requires Windows Vista or later

// POSIX constants for shutdown():
#  define SHUT_RD	SD_RECEIVE
#  define SHUT_WR	SD_SEND
#  define SHUT_RDWR	SD_BOTH


#else // Unix
// Link in libnsl, e.g. with "-lnsl" gcc option
#  include <sys/socket.h>
#  include <netinet/in.h> // sockaddr_in
#  include <arpa/inet.h> // inet_*, htonl, etc functions
#  include <netdb.h> // *addrinfo & getnameinfo functions, AI_*, EAI_*, NI_*, 

// Use the following names rather than values s.t. apps are compatible with Windows
#  define SOCKET int // Windows has distinct SOCKET type
#  define getSocketError() errno
#  define INVALID_SOCKET (-1) // Windows defines constant, but different value (0)
#  define SOCKET_ERROR (-1) // Windows defines constant & states functions return constant, but tries to hide value of constant.

#endif

#endif // TIM_SOCKETS_H
